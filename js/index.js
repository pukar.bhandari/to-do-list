let items = document.getElementById('todo');
let btn = document.getElementById('add');
let pending = document.getElementById('pending-tasks');
let completed = document.getElementById('completed-tasks');
let del = document.querySelectorAll('.delete');
let wrap = document.getElementById('wrapper');
let tasks = document.querySelector('.tasks');
let check = document.querySelectorAll('input[type=checkbox]');

for(let i=0;i<check.length;i++){
    if(check[i].parentNode.parentNode===completed){
        check[i].checked=true;
    }
}

function addItems() {
    const item = items.value;
    items.value = "";
    if (item === "") {
        return alert("Please enter something.");
    }
    pending.innerHTML += `<li class="allTasks clearfix">
    <input class="ptasks" type="checkbox" /> 
        <label for="ptasks">${item}</label>
        <button class="delete">&#10005;Delete</button></li>`;
    return pending;
}

wrap.addEventListener('click', function (ev) {
    if (ev.target && ev.target.matches('button.delete')) {
        ev.target.parentNode.remove();
    }
    if (ev.target && ev.target.matches('input[type=checkbox]')) {
        toggleStatus(ev.target);
    }
});

function toggleStatus(checkbox) {
    let clone = checkbox.parentElement.cloneNode(true);
    checkbox.parentNode.remove();
    checkbox.checked ? completed.appendChild(clone) : pending.appendChild(clone);
}

items.focus();
btn.addEventListener('click', addItems);
items.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        btn.click();
    }
});
tasks.addEventListener('dblclick', function (ev) {
    if (ev.target && ev.target.matches('label')) {
        const t = ev.target.innerHTML;
        ev.target.innerHTML = `<input type="text" value="${t}">`;
        ev.target.addEventListener('keyup', function (event) {
            if (event.key === "Enter") {
                ev.target.innerHTML = ev.target.querySelector('input').value;
                if (ev.target.innerHTML === "") {
                    ev.target.parentNode.remove();
                }
            }
        });
    }
});

